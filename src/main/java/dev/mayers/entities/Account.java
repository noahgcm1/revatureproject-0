package dev.mayers.entities;

public class Account {
	private int balance;
	private int accountID;
	private String accountName;
	
	public Account() {
		super();
		balance = 0;
	}
	
	public Account(String accountName) {
		this.accountName = accountName;
	}
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getAccountID() {
		return accountID;
	}
	
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}

	@Override
	public String toString() {
		return "Account [balance=" + balance + ", accountID=" + accountID + "]";
	}
}
