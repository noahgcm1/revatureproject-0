package dev.mayers.entities;

import java.sql.Timestamp;

public class Transaction implements Comparable<Transaction>{
	private int transactionID;
	private int amountExchanged;
	private int balanceAfter;
	private String timestamp;
	
	public Transaction() {
		super();
	}

	public Transaction(Account account, int amountExchanged) {
		super();
		this.amountExchanged = amountExchanged;

		//Transaction is performed on construction, afterward the object serves only to keep record
		this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
		this.balanceAfter = account.getBalance() + this.amountExchanged;
		account.setBalance(balanceAfter);
	}

	public int getAmountExchanged() {
		return amountExchanged;
	}

	public void setAmountExchanged(int amountExchanged) {
		this.amountExchanged = amountExchanged;
	}

	public int getBalanceAfter() {
		return balanceAfter;
	}

	public void setBalanceAfter(int balanceAfter) {
		this.balanceAfter = balanceAfter;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(int id) {
		this.transactionID = id;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + transactionID + ", amountExchanged=" + amountExchanged
				+ ", balanceAfter=" + balanceAfter + ", timestamp=" + timestamp + "]";
	}

	@Override
	public int compareTo(Transaction o) {
		return getTimestamp().compareTo(o.getTimestamp());
	}
}
