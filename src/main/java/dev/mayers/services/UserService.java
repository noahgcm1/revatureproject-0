package dev.mayers.services;

import java.util.List;

import dev.mayers.entities.*;
import dev.mayers.exceptions.*;

public interface UserService {
	 User registerUser(String username, String password) throws UsernameAlreadyExistsException, UsernameCharacterException, UsernameLengthException;
	
	User loginUser(String username, String password) throws  UsernameNotFoundException, IncorrectPasswordException;
	
	List<User> viewAllUsers();
	
	boolean updateUser(User user, String username, String password, boolean isSuperUser) throws UsernameAlreadyExistsException;
	
	User deleteUser(User user) throws AccountNotEmptyException;
	
	int numAccounts(String username);
	
	int amountInvested(String username) ;
	
	User getUser(String username);


	
	Account deleteAccount(User user, String accountName) throws AccountNotEmptyException;
	
	void createAccount(User user, String accountName) throws AccountAlreadyExistsException;
	
	List<Account> viewUserAccounts(User user);
	
	
	
	void doTransaction(User user, String accountName, int amountTransfered) throws AccountOverdrawnException;	
	
	List<Transaction> viewTransactionHistory(User user, String accountName);
}
