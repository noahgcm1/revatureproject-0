package dev.mayers.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dev.mayers.daos.*;
import dev.mayers.entities.*;
import dev.mayers.exceptions.*;

public class UserServiceImpl implements UserService{
	private static UserDAO ud = new UserDAOImpl();
	private static AccountDAO ad = new AccountDAOImpl();
	private static TransactionDAO td = new TransactionDAOImpl();
	
	/*
	 * Users
	 */
	public User registerUser(String username, String password)  throws UsernameAlreadyExistsException, UsernameCharacterException, UsernameLengthException{
		if(username.contains(" "))
			throw new UsernameCharacterException();
		if(username.length() < 7 || username.length() > 50)
			throw new UsernameLengthException();
		
		List<User> userSet = ud.getAllUsers();
		for(User u : userSet) 
			if(u.getUsername().equals(username))
				throw new UsernameAlreadyExistsException();
		
		User user = new User(username, password);
		ud.createUser(user);
		return ud.getUser(username);
	}

	public User loginUser(String username, String password) throws UsernameNotFoundException, IncorrectPasswordException {
		List<User> userSet = ud.getAllUsers();
		
		for(User u : userSet) {
			if(u.getUsername().equals(username))
				if(u.getPassword().equals(password))
					return u;
				else
					throw new IncorrectPasswordException();
		}
		
		throw new UsernameNotFoundException();
	}
	
	public boolean updateUser(User user, String username, String password, boolean isSuperUser) throws UsernameAlreadyExistsException {
		List<User> userSet = ud.getAllUsers();
		for(User u : userSet) 
			if(u.getUsername().equals(username) && ! user.getUsername().equals(username))
				throw new UsernameAlreadyExistsException();
		
		user.setUsername(username);
		user.setPassword(password);
		user.setSuperUser(isSuperUser);

		return ud.updateUser(user);
	}
	
	public List<User> viewAllUsers() {
		return ud.getAllUsers();
	}
	
	public int numAccounts(String username) {
		User user = ud.getUser(username);
		return ad.getUserAccounts(user).size();
	}
	
	public int amountInvested(String username) {
		
		User user = ud.getUser(username);
//		System.out.println(username);
//		System.out.println(ud.getUser("superadmin"));
//		System.out.println(ud.getUser(username));
//		System.out.println("userservice" + user + " " + username);
		List<Account> accounts = ad.getUserAccounts(user);
		
		int money = 0;
		for(Account a : accounts) 
			money += a.getBalance();
		
		return money;
	}
	
	public User deleteUser(User user) throws AccountNotEmptyException {
		List<Account> accounts = ad.getUserAccounts(user);
		for(Account a : accounts)
			if(a.getBalance() != 0)
				throw new AccountNotEmptyException();
		
		ud.deleteUser(user);
		return user;
	}
	
	public User getUser(String username) {
		return ud.getUser(username);
	}
	
	/*
	 * 	ACCOUNTS
	 */
	public void createAccount(User user, String accountName) throws AccountAlreadyExistsException {
		List<Account> accounts = ad.getUserAccounts(user);
		for(Account a : accounts)
			if(a.getAccountName().equals(accountName))
				throw new AccountAlreadyExistsException();
		
		ad.createAccount(user, new Account(accountName));
	}

	public Account deleteAccount(User user, String accountName) throws AccountNotEmptyException{
		Account account = ad.getAccount(user, accountName);
		if(account.getBalance() != 0.0)
			throw new AccountNotEmptyException();
		
		ad.deleteAccount(account);
		return account;
	}
	
	public List<Account> viewUserAccounts(User user){
		return ad.getUserAccounts(user);
	}

	
	/*
	 * TRANSACTIONS
	 */
	public void doTransaction(User user, String accountName, int amountTransferred)  throws AccountOverdrawnException {
		Account account = ad.getAccount(user, accountName);
		
		if((account.getBalance() + amountTransferred) < 0)
			throw new AccountOverdrawnException();
		
		td.createTransaction(account, new Transaction(account, amountTransferred));
	}
	
	public List<Transaction> viewTransactionHistory(User user, String accountName){
		List<Transaction> history = td.getAccountTransactions(ad.getAccount(user, accountName));
		Collections.sort(history);
		Collections.reverse(history);
		return history;
	}
}
