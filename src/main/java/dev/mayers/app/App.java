package dev.mayers.app;

import java.io.IOException;
import java.util.Scanner;

import dev.mayers.entities.*;
import dev.mayers.services.*;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application { 
		private static User loggedInUser = null;
		private static UserService service = new UserServiceImpl();
		private static Stage primaryStage;
		private static Class thisClass;
	
		@Override
	    public void start(Stage stage) throws Exception {
			primaryStage = stage;
			thisClass = getClass();
			
	        primaryStage.getIcons().add(new Image("/logo_icon.png"));
	        primaryStage.setTitle("RevatureOne Bank");
	        
	        setFXML("/Login.fxml");
		}
	   
	   	public static void main(String args[]){
	   		Scanner scan = new Scanner(System.in);
	   		String input = "";
	   		
	   		do {
	   			System.out.println("This part is a technicality! Enter \"LAUNCH\" to continue.");
	   			input = scan.next();
	   		} while(! input.toLowerCase().equals("launch"));
	   		
	   		launch(args);
	   	}
	   	
	   	public static User getLoggedInUser() {
	   		return loggedInUser;
	   	}
	   	
	   	public static void setLoggedInUser(User user) {
	   		loggedInUser = user;
	   	}
	
	   	public static UserService getService() {
			return service;
		}
		
	   	public static void setService(UserService service) {
			App.service = service;
		}
	
	   	public static void setFXML(String fileName) {
	   		Parent root;
			try {
				root = FXMLLoader.load(thisClass.getResource(fileName));
		        Scene scene = new Scene(root, 600, 400);
		        primaryStage.setScene(scene);
		        primaryStage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
	   	}
}