package dev.mayers.app;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import dev.mayers.entities.*;
import dev.mayers.exceptions.*;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;
import javafx.util.converter.IntegerStringConverter;

public class UserScreen implements Initializable{
	// fx:controller="dev.mayers.app.UserScreen"
	
	private static final String IDLE_BUTTON_STYLE = "-fx-background-color: transparent; -fx-border-color: #FFFFFF; -fx-border-radius: 3; -fx-font-family: futura-bold; -fx-text-fill: #FFFFFF";
	private static final String HOVER_BUTTON_STYLE = "-fx-background-color: #FFFFFF; -fx-border-color: #FFFFFF; -fx-border-radius: 3; -fx-background-radius: 3; -fx-font-family: futura-bold; -fx-text-fill: #F26926";

	@FXML HBox DropdownBar;
	@FXML HBox DropdownButtonBar;
	@FXML HBox TextButtonBar;
	@FXML HBox DropdownText2ButtonBar;

	@FXML ScrollPane MainPane;
	
	@FXML Button ViewAllAccountsButton;
	@FXML Button CreateAccountButton;
	@FXML Button ViewHistoryButton;
	@FXML Button NewTransactionButton;
	@FXML Button UsersButton;
	@FXML Button ViewAllUsersButton;
	@FXML Button EditUserDetailsButton;
	@FXML Button LogoutButton;
	
	@FXML Button DropdownButtonBarButton;
	@FXML Button TextButtonBarButton;
	@FXML Button DropdownText2ButtonBarButton1;
	@FXML Button DropdownText2ButtonBarButton2;
	@FXML Button UpdateUserButton;
	@FXML Button CreateUserButton;
	
	@FXML Label UserLabel;
	@FXML Label DropdownButtonBarErrorLabel;
	@FXML Label DropdownText2ButtonBarErrorLabel;
	@FXML Label TextButtonBarErrorLabel;
	@FXML Label MainDisplayLabel;
	@FXML Label UpdateUserErrorLabel;
	
	@FXML ComboBox<String> DropdownBarComboBox;
	@FXML ComboBox<String> DropdownButtonBarComboBox;
	@FXML ComboBox<String> DropdownText2ButtonBarComboBox;
	@FXML ComboBox<String> SuperUserDropdown;
	
	@FXML TextField TextButtonBarText;
	@FXML TextField DropdownText2ButtonBarText;
	@FXML TextField UserUsername;
	@FXML TextField UserPassword;
	
	public void initialize(URL location, ResourceBundle resources) {
		UserLabel.setText(App.getLoggedInUser().getUsername());
		
        if(! App.getLoggedInUser().isSuperUser()) {
        	UsersButton.setVisible(false);
        	ViewAllUsersButton.setVisible(false);
        	EditUserDetailsButton.setVisible(false);
        }
        LogoutButton.setOnMouseEntered(e -> LogoutButton.setStyle(HOVER_BUTTON_STYLE));
        LogoutButton.setOnMouseExited(e -> LogoutButton.setStyle(IDLE_BUTTON_STYLE));
        LogoutButton.setOnAction(e -> App.setFXML("/Login.fxml"));
        
        ViewAllAccountsButton.setOnAction(e -> viewAllAccounts());
        CreateAccountButton.setOnAction(e -> createAccount());
        
        ViewHistoryButton.setOnAction(e -> viewHistory());
        NewTransactionButton.setOnAction(e -> newTransaction());
        
        ViewAllUsersButton.setOnAction(e -> viewAllUsers());
        EditUserDetailsButton.setOnAction(e -> editUserDetails());
        
        viewAllAccounts();
	}
	
	private void setBar(int barIndex) {
		HBox[] bars = {DropdownBar, DropdownButtonBar, TextButtonBar, DropdownText2ButtonBar};
		MainPane.setVisible(true);
		
		for(int x = 0; x < bars.length; x++) {
			bars[x].setVisible(x == barIndex);	
		}
	}

	/*
	 * VIEW ALL ACCOUNTS
	 */
	private void viewAllAccounts() {
		setBar(1);
		DropdownButtonBarButton.setText("Delete");
		
		DropdownButtonBarComboBox.getItems().clear();
		DropdownButtonBarComboBox.getItems().add("All Accounts");
		DropdownButtonBarComboBox.getSelectionModel().selectFirst();
		
		List<Account> accountList = App.getService().viewUserAccounts(App.getLoggedInUser());
		for(Account a : accountList) 
			DropdownButtonBarComboBox.getItems().add(a.getAccountName());
		
		DropdownButtonBarComboBox.setOnAction(e -> viewAccountUpdate(DropdownButtonBarComboBox.getValue()));
		DropdownButtonBarButton.setOnAction(e -> viewAccountDelete(DropdownButtonBarComboBox.getValue()));
		viewAccountUpdate("All Accounts");
	}
	
	private void viewAccountUpdate(String choice) {
		try {
			DropdownButtonBarErrorLabel.setText("");
			List<Account> accountList = App.getService().viewUserAccounts(App.getLoggedInUser());
			
			if(choice.equals("All Accounts")) {
				String allAccounts = "";
				for(Account a : accountList)
					allAccounts += "Account Name: \""+a.getAccountName()+"\"\nBalance: $"+a.getBalance()+"\n\n";
				MainDisplayLabel.setText(allAccounts);
				
			} else {
				for(Account a : accountList)
					if(choice.equals(a.getAccountName()))
						MainDisplayLabel.setText("Account Name: \""+a.getAccountName()+"\"\nBalance: $"+a.getBalance()+"\n\n");
			}
		} catch(NullPointerException npe) {}
	}
	
	private void viewAccountDelete(String choice) {
		if(choice.equals("All Accounts")) {
			DropdownButtonBarErrorLabel.setText("NO ACCOUNT SELECTED");
		}else {
			if(! (choice.equals(null) || choice.equals(""))) {
				try {
					App.getService().deleteAccount(App.getLoggedInUser(), choice);
					System.out.println("Deleted account: " + choice);
					viewAllAccounts();
				} catch(AccountNotEmptyException anee) {
					DropdownButtonBarErrorLabel.setText("ACCOUNT NOT EMPTY");
				}
			}
		}
	}

	/*
	 * CREATE ACCOUNT
	 */
	private void createAccount() {
		setBar(2);
		viewAccountUpdate("All Accounts");
		
		TextButtonBarText.setPromptText("New Account Name");
		TextButtonBarButton.setOnAction(e -> createAccountSubmit(TextButtonBarText.getText()));
	}
	
	private void createAccountSubmit(String accountName) {
		try {
			App.getService().createAccount(App.getLoggedInUser(), accountName);
			System.out.println("Created account: "+accountName);
			viewAccountUpdate("All Accounts");
		}catch(AccountAlreadyExistsException aaee) {
			TextButtonBarErrorLabel.setText("ACCOUNT NAME ALREADY EXISTS");
		}
	}
	
	/*
	 * VIEW HISTORY
	 */
	private void viewHistory() {
		setBar(0);
		DropdownBarComboBox.getItems().clear();
		
		List<Account> accountList = App.getService().viewUserAccounts(App.getLoggedInUser());
		for(Account a : accountList) 
			DropdownBarComboBox.getItems().add(a.getAccountName());
		DropdownBarComboBox.getSelectionModel().selectFirst();
		
		DropdownBarComboBox.setOnAction(e -> viewHistoryUpdate(DropdownBarComboBox.getValue()));
		viewHistoryUpdate(DropdownBarComboBox.getValue());
	}
	
	private void viewHistoryUpdate(String choice) {
		try {
			List<Transaction> transactionList = App.getService().viewTransactionHistory(App.getLoggedInUser(), choice);
			String history = "";
			for(Transaction t : transactionList)
				history += t.getTimestamp() + 
						"\n" + (t.getAmountExchanged() > 0 ? "Deposited: $" : "Withdrew: $") + Math.abs(t.getAmountExchanged()) + 
						"\nBalance:" + t.getBalanceAfter() + "\n\n";
			MainDisplayLabel.setText(history);
		}catch(NullPointerException npe) {}
	}
	
	/*
	 * NEW TRANSACTION
	 */
	private void newTransaction() {
		setBar(3);
		DropdownText2ButtonBarText.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
		
		DropdownText2ButtonBarComboBox.getItems().clear();
		List<Account> accountList = App.getService().viewUserAccounts(App.getLoggedInUser());
		for(Account a : accountList) 
			DropdownText2ButtonBarComboBox.getItems().add(a.getAccountName());
		DropdownText2ButtonBarComboBox.getSelectionModel().selectFirst();
		
		DropdownText2ButtonBarButton1.setOnAction(e -> doTransaction(DropdownText2ButtonBarComboBox.getValue(), Integer.parseInt(DropdownText2ButtonBarText.getText())));
		DropdownText2ButtonBarButton2.setOnAction(e -> doTransaction(DropdownText2ButtonBarComboBox.getValue(), (-1 * Integer.parseInt(DropdownText2ButtonBarText.getText()))));
		
		DropdownText2ButtonBarComboBox.setOnAction(e -> viewHistoryUpdate(DropdownText2ButtonBarComboBox.getValue()));
		viewHistoryUpdate(DropdownText2ButtonBarComboBox.getValue());
	}
	
	private void doTransaction(String choice, int amount) {
		try {
			DropdownText2ButtonBarErrorLabel.setText("");
			App.getService().doTransaction(App.getLoggedInUser(), choice, amount);
			System.out.println("$"+Math.abs(amount) + (amount < 0 ? " withdrawn from: " : " deposited into: ") + choice);
			viewHistoryUpdate(choice);
		} catch(AccountOverdrawnException aoe) {
			DropdownText2ButtonBarErrorLabel.setText("OVERDRAWN");
		}
	}
	
	/*
	 * VIEW ALL USERS
	 */
	private void viewAllUsers() {
		setBar(0);
		
		DropdownBarComboBox.getItems().clear();
		DropdownBarComboBox.getItems().add("All Users");
		DropdownBarComboBox.getSelectionModel().selectFirst();
		
		List<User> userList = App.getService().viewAllUsers();
		for(User u : userList) 
			DropdownBarComboBox.getItems().add(u.getUsername());
		
		viewUserUpdate("All Users");
		DropdownBarComboBox.setOnAction(e -> viewUserUpdate(DropdownBarComboBox.getValue()));
	}
	
	private void viewUserUpdate(String choice) {
		System.out.println(choice);
		try {
			List<User> userList = App.getService().viewAllUsers();
			
			if(choice.equals("All Users")) {
				String allUsers = "";
				
				for(User u : userList) {
					allUsers += "Username: \"" + u.getUsername() + "\"\nPassword: \"" + u.getPassword() + "\"\nSuperUser: " + u.isSuperUser() + "\nAccounts: " + 
						App.getService().numAccounts(u.getUsername()) + "\nTotal Money Invested: $" + App.getService().amountInvested(u.getUsername()) + "\n\n";
				}
				
				System.out.println(allUsers);
				MainDisplayLabel.setText(allUsers);
				
			} else {
				for(User u : userList)
					if(choice.equals(u.getUsername()))
						MainDisplayLabel.setText("Username: \"" + u.getUsername() + "\"\nPassword: \"" + u.getPassword() + "\"\nSuperUser: " + u.isSuperUser() + "\nAccounts: " + 
								App.getService().numAccounts(u.getUsername()) + "\nTotal Money Invested: $" + App.getService().amountInvested(u.getUsername()) + "\n\n");
			}
		} catch(NullPointerException npe) {
			npe.printStackTrace();
		}
	}
	
	/*
	 * EDIT USER DETAILS
	 */
	private void editUserDetails() {
		setBar(1);
		MainPane.setVisible(false);
		
		SuperUserDropdown.getItems().clear();
		SuperUserDropdown.getItems().addAll("true","false");
		
		DropdownButtonBarComboBox.getItems().clear();
		List<User> userList = App.getService().viewAllUsers();
		for(User u : userList) 
			DropdownButtonBarComboBox.getItems().add(u.getUsername());
		DropdownButtonBarComboBox.setOnAction(e -> editUserUpdate(DropdownButtonBarComboBox.getValue()));
		DropdownButtonBarComboBox.getSelectionModel().selectFirst();
		
		DropdownButtonBarButton.setOnAction(e -> editUserDelete(DropdownButtonBarComboBox.getValue()));
		UpdateUserButton.setOnAction(e -> editUserSubmit());
		CreateUserButton.setOnAction(e -> editUserCreate());
	}
	
	private void editUserUpdate(String choice) {
		try {
			UpdateUserErrorLabel.setText("");
			
			User user = App.getService().getUser(choice);
			UserUsername.setText(user.getUsername());
			UserPassword.setText(user.getPassword());
			SuperUserDropdown.setValue(user.isSuperUser() ? "true" : "false");
		} catch (NullPointerException npe) {}
	}
	
	private void editUserDelete(String choice) {
		try {
			UpdateUserErrorLabel.setText("");
			
			App.getService().deleteUser(App.getService().getUser(choice));
			System.out.println("User deleted: " + choice);
			editUserDetails();
		} catch (AccountNotEmptyException e) {
			UpdateUserErrorLabel.setText("User has money still held in open accounts");
		}
	}
	
	private void editUserSubmit() {
		try {
			UpdateUserErrorLabel.setText("");
			
			User user = App.getService().getUser(DropdownButtonBarComboBox.getValue());
			String username = UserUsername.getText();
			String password = UserPassword.getText();
			boolean superuser = Boolean.parseBoolean(SuperUserDropdown.getValue());
			App.getService().updateUser(user, username, password, superuser);
			System.out.println("User updated: " + username);
		} catch (UsernameAlreadyExistsException e) {
			UpdateUserErrorLabel.setText("A user by that username already exists");
		}
	}
	
	private void editUserCreate() {
		try {
			UpdateUserErrorLabel.setText("");

			String username = UserUsername.getText();
			String password = UserPassword.getText();
			App.getService().registerUser(username, password);
			System.out.println("User created: " + username);
		} catch (UsernameAlreadyExistsException uaee) {
			UpdateUserErrorLabel.setText("A user by that username already exists");
		} catch(UsernameCharacterException uce) {
			UpdateUserErrorLabel.setText("Invalid character in userneame");
		} catch(UsernameLengthException ule) {
			UpdateUserErrorLabel.setText("Username length must be between 7 and 50 characters");
		}
	}
}