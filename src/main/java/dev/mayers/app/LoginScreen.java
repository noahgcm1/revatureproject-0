package dev.mayers.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import dev.mayers.exceptions.IncorrectPasswordException;
import dev.mayers.exceptions.UsernameAlreadyExistsException;
import dev.mayers.exceptions.UsernameCharacterException;
import dev.mayers.exceptions.UsernameLengthException;
import dev.mayers.exceptions.UsernameNotFoundException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginScreen implements Initializable{
	private static final String IDLE_BUTTON_STYLE = "-fx-background-color: transparent; -fx-border-color: #FFFFFF; -fx-border-radius: 3; -fx-font-family: futura-bold; -fx-text-fill: #FFFFFF";
	private static final String HOVER_BUTTON_STYLE = "-fx-background-color: #FFFFFF; -fx-border-color: #FFFFFF; -fx-border-radius: 3; -fx-background-radius: 3; -fx-font-family: futura-bold; -fx-text-fill: #F26926";
	
	@FXML Button loginButton;
	@FXML Button registerButton;
	@FXML TextField  usernameField;
	@FXML PasswordField passwordField;
	@FXML Label promptLabel;

	public void initialize(URL location, ResourceBundle resources) {
		setButtonHoverable(loginButton);
		setButtonHoverable(registerButton);
		
		loginButton.setOnAction(e -> login());
		registerButton.setOnAction(e -> swapToRegister());
	}
	
	private void setButtonHoverable(Button button) {
		button.setStyle(IDLE_BUTTON_STYLE);
        button.setOnMouseEntered(e -> button.setStyle(HOVER_BUTTON_STYLE));
        button.setOnMouseExited(e -> button.setStyle(IDLE_BUTTON_STYLE));
    }
	
	private void login() {
		try {
			//usernameField.setText("");
			//passwordField.setText("");
			String user = usernameField.getText();
			String pass = passwordField.getText();
			
			if(! (user.equals("") || pass.equals(""))) {
				App.setLoggedInUser(App.getService().loginUser(usernameField.getText(), passwordField.getText()));
				System.out.println("Logged in as: " + App.getLoggedInUser().getUsername());
				toMainScreen();
			}
		} catch (IncorrectPasswordException ipe) {
			promptLabel.setText("ERROR: incorrect password.");
		} catch (UsernameNotFoundException unfe) {
			promptLabel.setText("ERROR: username not found.");
		}
	}

	private void swapToLogin() {
		promptLabel.setText("Log in below or register to get started");
		
		loginButton.setText("Log in");
		loginButton.setOnAction(e -> login());
		
		registerButton.setText("Don't have an account? Register now!");
		registerButton.setOnAction(e -> swapToRegister());
	}
	
	private void register() {
		try {
			//usernameField.setText("");
			//passwordField.setText("");
			String user = usernameField.getText();
			String pass = passwordField.getText();
			
			if(! (user.equals("") || pass.equals(""))) {
				App.setLoggedInUser(App.getService().registerUser(user, pass));
				System.out.println("Registered user: " + App.getLoggedInUser().getUsername());
				toMainScreen();
			}
		} catch (UsernameCharacterException uce) {
			promptLabel.setText("ERROR: username cannot contain spaces");
		}catch(UsernameLengthException ule) {
			promptLabel.setText("ERROR: usernames must be between 7 and 50 characters");
		}catch(UsernameAlreadyExistsException uaee) {
			promptLabel.setText("ERROR: username already taken");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	private void swapToRegister() {
		promptLabel.setText("Enter username and password for new account");
		
		loginButton.setText("Create");
		loginButton.setOnAction(e -> register());
		
		registerButton.setText("Already have an account? Log in here!");
		registerButton.setOnAction(e -> swapToLogin());
	}

	private void toMainScreen() { 
		App.setFXML("/Main.fxml");
    }
}