package dev.mayers.daos;

import java.util.List;
import dev.mayers.entities.Account;
import dev.mayers.entities.User;

public interface AccountDAO {
		//CREATE
		boolean createAccount(User user, Account account);
		
		//READ
		Account getAccount(int id);
		Account getAccount(User user, String name);
		List<Account> getAllAccounts();
		List<Account> getUserAccounts(User user);
		
		//UPDATE
		boolean updateAccount(Account account);
		
		//DELETE
		boolean deleteAccount(Account account);
}