package dev.mayers.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import dev.mayers.entities.User;
import dev.mayers.entities.Account;

public class AccountDAOImpl implements AccountDAO{

	public static Connection conn = JDBCConnection.getConnection();

	/*
	 * CREATE
	 */
	public boolean createAccount(User user, Account account) {
		try {
	    	String sql = "CALL add_account(?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, Integer.toString(user.getUserID()));
	    	cs.setString(2, account.getAccountName());
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
        return false;
	}

	/*
	 * READ
	 */
	public Account getAccount(int id) {
		try {
	    	String sql = "SELECT * FROM account_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Account a = new Account();
                a.setAccountID(rs.getInt("ID"));
                a.setAccountName(rs.getString("NAME"));
                a.setBalance(Integer.parseInt("BALANCE"));
                return a;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

	public Account getAccount(User user, String name) {
		try {
	    	String sql = "SELECT * FROM account_table WHERE user_id = ? AND name = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(user.getUserID()));
	        ps.setString(2, name);
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Account a = new Account();
                a.setAccountID(rs.getInt("ID"));
                a.setAccountName(rs.getString("NAME"));
                a.setBalance(rs.getInt("BALANCE"));
                return a;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

	public List<Account> getAllAccounts() {
		try {			
    		String sql = "SELECT * FROM account_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Account> accounts = new ArrayList<Account>();		
    		while(rs.next()) {
    			Account a = new Account();
                a.setAccountID(rs.getInt("ID"));
                a.setAccountName(rs.getString("NAME"));
                a.setBalance(rs.getInt("BALANCE"));
    			accounts.add(a);
    		}			
    		return accounts;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	public List<Account> getUserAccounts(User user) {
		try {			
    		String sql = "SELECT * FROM account_table WHERE user_id = ?";
    		//System.out.println("accountdao "+user);
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, Integer.toString(user.getUserID()));
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Account> accounts = new ArrayList<Account>();		
    		while(rs.next()) {
    			Account a = new Account();
                a.setAccountID(rs.getInt("ID"));
                a.setAccountName(rs.getString("NAME"));
                a.setBalance(rs.getInt("BALANCE"));
    			accounts.add(a);
    		}			
    		return accounts;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * UPDATE
	 */
	public boolean updateAccount(Account account) {
		try {
			String sql = "UPDATE account_table SET name = ?, balance = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, account.getAccountName());
			ps.setString(2, Integer.toString(account.getBalance()));
			ps.setString(3, Integer.toString(account.getAccountID()));
			ps.executeQuery();
			
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * DELETE
	 */
	public boolean deleteAccount(Account account) {
		try {
			String sql = "DELETE FROM account_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getAccountID());
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}
