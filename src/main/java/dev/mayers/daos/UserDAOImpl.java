package dev.mayers.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.mayers.entities.*;

public class UserDAOImpl implements UserDAO {

    public static Connection conn = JDBCConnection.getConnection();
    
    /*
     * CREATE
     */
    public boolean createUser(User user) {
    	try {
	    	String sql = "CALL add_user(?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, user.getUsername());
	    	cs.setString(2, user.getPassword());
	    	cs.execute();
	    	
	    	return true;
	    	
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
        return false;
    }
    
    /*
     * READ
     */
    public User getUser(int id) {
    	return null;
    }
        
    public User getUser(String username) {
    	try {
	    	String sql = "SELECT * FROM user_table WHERE username = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, username);
	        ResultSet rs = ps.executeQuery();

	        if(rs.next()) {
                User u = new User();
                u.setUserID(rs.getInt("ID"));
                u.setUsername(rs.getString("USERNAME"));
                u.setPassword(rs.getString("PASSWORD"));
                u.setSuperUser(rs.getInt("SUPER") == 1);
                //System.out.println(u);
                return u;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
    }
        
    public List<User> getAllUsers() {
    	try {			
    		String sql = "SELECT * FROM user_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<User> users = new ArrayList<User>();		
    		while(rs.next()) {
    			User u = new User();
    			u.setUserID(rs.getInt("ID"));
    			u.setSuperUser(rs.getInt("SUPER") == 1);
    			u.setUsername(rs.getString("USERNAME"));
    			u.setPassword(rs.getString("PASSWORD"));
    			users.add(u);
    		}			
    		return users;		
    		
    	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
}

    /*
     * UPDATE
     */
    public boolean updateUser(User update) {
    	try {
			String sql = "UPDATE user_table SET username = ?, password = ?, super = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, update.getUsername());
			ps.setString(2, update.getPassword());
			ps.setString(3, update.isSuperUser() ? "1" : "0");
			ps.setString(4, Integer.toString(update.getUserID()));
			ps.executeQuery();
			
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /*
     * DELETE
     */
	public boolean deleteUser(User user) {
		try {
			String sql = "DELETE FROM user_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getUserID());
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}