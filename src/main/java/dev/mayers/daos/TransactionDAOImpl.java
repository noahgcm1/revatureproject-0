package dev.mayers.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import dev.mayers.entities.Account;
import dev.mayers.entities.Transaction;

public class TransactionDAOImpl implements TransactionDAO{

	public static Connection conn = JDBCConnection.getConnection();
	
	/*
	 * CREATE
	 */
	public boolean createTransaction(Account account, Transaction transaction) {
		try {
	    	String sql = "CALL add_transaction(?,?,?)";
	    	CallableStatement cs = conn.prepareCall(sql);
	    	cs.setString(1, Integer.toString(account.getAccountID()));
	    	cs.setString(2, Integer.toString(transaction.getAmountExchanged()));
	    	cs.setString(3, Integer.toString(transaction.getBalanceAfter()));
	    	cs.execute();
	    	
	    	new AccountDAOImpl().updateAccount(account);
	    	return true;
	    	
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
        return false;
	}

	/*
	 * READ
	 */
	public Transaction getTransaction(int id) {
		try {
	    	String sql = "SELECT * FROM transaction_table WHERE id = ?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, Integer.toString(id));
	        ResultSet rs = ps.executeQuery();
	        
	        if(rs.next()) {
                Transaction t = new Transaction();
                t.setTransactionID(rs.getInt("ID"));
                t.setAmountExchanged(rs.getInt("AMMOUNT_EXCHANGED"));
                t.setBalanceAfter(rs.getInt("BALANCE_AFTER"));
                t.setTimestamp(rs.getString("TIMESTAMP"));
                return t;
	        }
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return null;
	}

	public List<Transaction> getAllTransactions() {
		try {			
    		String sql = "SELECT * FROM account_table";
    		PreparedStatement ps = conn.prepareStatement(sql);			
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Transaction> transactions = new ArrayList<Transaction>();		
    		while(rs.next()) {
    			Transaction t = new Transaction();
                t.setTransactionID(rs.getInt("ID"));
                t.setAmountExchanged(rs.getInt("AMMOUNT_EXCHANGED"));
                t.setBalanceAfter(rs.getInt("BALANCE_AFTER"));
                t.setTimestamp(rs.getString("TIMESTAMP"));
    			transactions.add(t);
    		}			
    		return transactions;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	public List<Transaction> getAccountTransactions(Account account) {
		try {			
    		String sql = "SELECT * FROM transaction_table WHERE account_id = ?";
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setString(1, Integer.toString(account.getAccountID()));
    		ResultSet rs = ps.executeQuery();			
    		
    		List<Transaction> transactions = new ArrayList<Transaction>();		
    		while(rs.next()) {
    			Transaction t = new Transaction();
                t.setTransactionID(rs.getInt("ID"));
                t.setAmountExchanged(rs.getInt("AMMOUNT_EXCHANGED"));
                t.setBalanceAfter(rs.getInt("BALANCE_AFTER"));
                t.setTimestamp(rs.getString("TIMESTAMP"));
    			transactions.add(t);
    		}			
    		return transactions;		
    		
    	} catch (SQLException e) {
    		e.printStackTrace();
		}
		return null;
	}

	/*
	 * UPDATE
	 */
	public boolean updateTransaction(Transaction transaction) {
		try {
			String sql = "UPDATE transaction_table SET ammount_exchanged = ?, balance_after = ?, timestamp = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(transaction.getAmountExchanged()));
			ps.setString(2, Integer.toString(transaction.getBalanceAfter()));
			ps.setString(3, transaction.getTimestamp());
			ps.setString(4, Integer.toString(transaction.getTransactionID()));
			ps.executeQuery();
			
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
 
	/*
	 * DELETE
	 */
	public boolean deleteTransaction(Transaction transaction) {
		try {
			String sql = "DELETE FROM transaction_table WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, transaction.getTransactionID());
			ps.executeQuery();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}
