package dev.mayers.daos;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;


public class JDBCConnection {
	/*
	 * We are going to maintain and observe a single connection object within this class. If no connection exists, we will create one.
	 * If a connection does exist, make a new one and return it.
	 */
	
	private static Connection conn = null;
	
	public static Connection getConnection() {
		
		try {
			if(conn==null) {
				/*
				 *  Oracle added a *hotfix* to ensure that ojdbc drivers would correctly load
				 *  at the beginning of your application starting
				 */
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				//to establish a connection we need: url(endpoint), username, password
				Properties props = new Properties();
				FileInputStream input = new FileInputStream(JDBCConnection.class.getClassLoader().getResource("connection.properties").getFile());
				props.load(input);				
				
				String url = props.getProperty("url");
				String username = props.getProperty("username");
				String password = props.getProperty("password");
				
//				String endpoint="mayers-testdb.cryr8sy5pqb6.us-east-1.rds.amazonaws.com";
//				String url = "jdbc:oracle:thin:@" + endpoint + ":1521:ORCL";
//				String	username = "admin";
//				String password = "superpass";
				
				conn = DriverManager.getConnection(url, username, password);
				return conn;
				
			} else {		
				return conn;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		
		Connection conn1 = getConnection();
		
		System.out.println(conn1);
	}
}