package dev.mayers.daos;

import java.util.List;
import dev.mayers.entities.User;

public interface UserDAO {
	
	//CREATE
	boolean createUser(User user);
	
	//READ
	User getUser(int id);
	User getUser(String username);
	List<User> getAllUsers();
	
	//UPDATE
	boolean updateUser(User user);
	
	//DELETE
	boolean deleteUser(User user);
}
