package dev.mayers.daos;

import java.util.List;
import dev.mayers.entities.Transaction;
import dev.mayers.entities.Account;

public interface TransactionDAO {
		//CREATE
		boolean createTransaction(Account account, Transaction transaction);
		
		//READ
		Transaction getTransaction(int id);
		List<Transaction> getAllTransactions();
		List<Transaction> getAccountTransactions(Account account);
		
		//UPDATE
		boolean updateTransaction(Transaction transaction);
		
		//DELETE
		boolean deleteTransaction(Transaction transaction);
}