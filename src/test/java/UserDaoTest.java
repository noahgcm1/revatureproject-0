import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import dev.mayers.daos.UserDAO;
import dev.mayers.daos.UserDAOImpl;
import dev.mayers.entities.User;

class UserDaoTest {
	UserDAO ud;
	User testUser;
	User testSuperUser;
	
	@BeforeAll
	void setup() {
		ud = new UserDAOImpl();
	}
	
	@Test
	@Order(1)
	void createUserTest() {
		ud.createUser(null);
	}

}
